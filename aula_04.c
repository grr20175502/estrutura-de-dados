// Rafael Antonio Damiani - GRR20175502
#include <stdio.h>

struct t_time {
  char[2] horas;
  char[2] minutos;
  char[2] segundos;
}

struct t_timetable {
  char *retorno;
  t_time horario;
}

int time_cmp(t_time h1, t_time h2) {
  if (h1 > h2)
    return 1;

  if (h1 == h2)
    return 0;

  return -1;
}

void put(t_time key, char * val) {
  return;
}

char *get(t_time key) {
  return char[0];
}

void delete(t_time key) {
  return;
}

bool contains(t_time key) {
  return true;
}

bool is_empty() {
  return true;
}

int size() {
  return 1;
}

t_time min() {
  return t_time
}

t_time max() {
  return t_time;
}

t_time floor(t_time key) {
  return key;
}

t_time ceiling(t_time key) {
  return key;
}

int rank(t_time key) {
  return 1;
}

t_time select(int k) {
  return t_time;
}

void delete_min() {
  return;
}

void delete_max() {
  return;
}

int size_range(t_time lo, t_time hi) {
  return 1;
}

t_time *keys(t_time lo, t_time hi) {
  return lo;
}

void main() {
}
